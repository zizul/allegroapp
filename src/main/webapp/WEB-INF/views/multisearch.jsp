
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Allegro Multi Search</title>
<style>
body {
    background-color: linen;
}
table th {
	text-align:left;
}
</style>
</head>
<body>
	
	<c:url var="multisearch" value="/multisearch"></c:url>
	<h1>Wyszukiwarka sprzedawców posiadających kilka przedmiotów.</h1>
	<form:form action="${multisearch}" method="POST"
		id="multi-search-form">
		<span id="title"> <label for="searchString">Wpisz
				poszukiwane nazwy oddzielone przecinkami (np. myszka, komputer,
				monitor): </label> <input id="searchString" name="searchString" size="30"
			maxlength="80" />
		</span>
		<span id="form-actions">
			<button>Szukaj</button>
		</span>
	</form:form>

	<c:if test="${!empty error}">
		<h2>Wyniki:</h2>
		<c:if test="${error == 'NOT_FOUND'}">
			<div>Nie znaleziono odpowiednich sprzedawców.</div>
		</c:if>
		<c:if test="${error == 'NOT_VALID'}">
			<div>Poszukiwana fraza jest nieprawidłowa.</div>
		</c:if>
	</c:if>

<%-- 	<c:url var="next" value="/next?page=2&results=10"></c:url> --%>
<%-- 	<a href="${next}">Następne</a> --%>
<%-- 	${next} --%>

	<c:if test="${!empty sellers}">

		<h2>Wyniki dla frazy "${searchString}":</h2>
		<table>
			<tr>
				<th>Nr</th>
				<th>Login sprzedawcy</th>
				<th>Strona sprzedawcy</th>
			</tr>
			<c:forEach items="${sellers}" var="seller" varStatus="loop">
				<tr>
					<td>${loop.index + 1}.</td>
					<td>${seller.login}</td>
					<td><a href="http://allegro.pl/show_user.php?uid=${seller.id}">
							http://allegro.pl/show_user.php?uid=${seller.id} </a></td>
				</tr>
			</c:forEach>
		</table>
	</c:if>
</body>
</html>