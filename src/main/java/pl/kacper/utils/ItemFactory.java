package pl.kacper.utils;

import pl.kacper.spring.domain.Item;
import pl.kacper.spring.domain.Seller;
import pl.kacper.wsdl.ItemsListType;

public class ItemFactory {

	public static Item getItem(ItemsListType itemsListType) {
		Item item = new Item();
		item.setId(itemsListType.getItemId());
		item.setTitle(itemsListType.getItemTitle());
		Seller seller = SellerFactory.getSeller(itemsListType);
		item.setSeller(seller);
		return item;
	}
}
