package pl.kacper.utils;

import pl.kacper.spring.domain.Seller;
import pl.kacper.wsdl.ItemsListType;

public class SellerFactory {

	public static Seller getSeller(ItemsListType itemsListType) {
		Seller seller = new Seller();
		if(itemsListType.getSellerInfo() != null)
			seller.setId(itemsListType.getSellerInfo().getUserId());
			seller.setLogin(itemsListType.getSellerInfo().getUserLogin());
		return seller;
	}
}
