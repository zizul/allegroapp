package pl.kacper.ws;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import pl.kacper.wsdl.ArrayOfFilteroptionstype;
import pl.kacper.wsdl.ArrayOfString;
import pl.kacper.wsdl.DoGetItemsListRequest;
import pl.kacper.wsdl.DoGetItemsListResponse;
import pl.kacper.wsdl.FilterOptionsType;

@Service
public class WebserviceClient<Req, Res> extends WebServiceGatewaySupport {

	private static final Logger logger = LoggerFactory
			.getLogger(WebserviceClient.class);

	@Value("${ws.webkey}")
	private String webkey;
	// @Value("${ws.password}")
	// private String pass;
	// @Value("${ws.user}")
	// private String user;
	@Value("${ws.countrycode}")
	private int countryCode;

	@Autowired
	public void setWebServiceTemp(WebServiceTemplate webServiceTemplate) {
		setWebServiceTemplate(webServiceTemplate);
	}

	@SuppressWarnings("unchecked")
	public Res send(Req request) {
		return (Res) getWebServiceTemplate().marshalSendAndReceive(request);
	}

	public String getWebkey() {
		return webkey;
	}

	public int getCountryCode() {
		return countryCode;
	}

}
