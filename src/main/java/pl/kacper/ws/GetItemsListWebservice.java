package pl.kacper.ws;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kacper.wsdl.DoGetItemsListRequest;
import pl.kacper.wsdl.DoGetItemsListResponse;

@Service
public class GetItemsListWebservice extends WebserviceClient<DoGetItemsListRequest, DoGetItemsListResponse> {

	private static final Logger logger = LoggerFactory.getLogger(GetItemsListWebservice.class);
	
	@Autowired
	private RequestFilterOptionsService requestFilterOptionsService;
	
	public DoGetItemsListResponse getResponse(HashMap<String, List<String>> parameters) {
		int size = 1000;
		int offset = 0;
		int counter = 0;
		
		DoGetItemsListRequest request = new DoGetItemsListRequest();
		request.setWebapiKey(getWebkey());
		request.setCountryId(getCountryCode());
		request.setResultSize(size);
		request.setFilterOptions(requestFilterOptionsService.getFilterOptions(parameters));
		
		DoGetItemsListResponse response = send(request);
		if(response != null && response.getItemsList() != null) {
			counter = response.getItemsList().getItem().size();
			offset += counter;
			request.setResultOffset(offset);
			logger.debug(offset + " of " + response.getItemsCount() + " fetched.");
		}
		while(counter > 0) {
			DoGetItemsListResponse responseNext = send(request);
			if(responseNext.getItemsList() != null) {
				counter = responseNext.getItemsList().getItem().size();
				offset += counter;
				response.getItemsList().getItem().addAll(responseNext.getItemsList().getItem());
				logger.debug(offset + " of " + response.getItemsCount() + " fetched.");
			} else
				counter = 0;
			request.setResultOffset(offset);
		}
		logger.debug("\"" + parameters.get(RequestFilterOptionsService.SEARCH_FILTER_ID) +"\"  items count: " + response.getItemsCount() + ", items offset: " + offset);
		
		return response;
		
	}

}
