package pl.kacper.ws;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Service;

import pl.kacper.wsdl.ArrayOfFilteroptionstype;
import pl.kacper.wsdl.ArrayOfString;
import pl.kacper.wsdl.FilterOptionsType;
import pl.kacper.wsdl.RangeValueType;

@Service
public class RequestFilterOptionsService {

	public static String SEARCH_FILTER_ID = "search";
	public static String USERID_FILTER_ID = "userId";
	public static String PRICE_FILTER_ID = "price";
	
	public ArrayOfFilteroptionstype getFilterOptions(HashMap<String, List<String>> parameters) {
		ArrayOfFilteroptionstype filterOptions = new ArrayOfFilteroptionstype();
		FilterOptionsType optionsType = new FilterOptionsType();
		for (String filterId : parameters.keySet()) {
			optionsType = new FilterOptionsType();
			if(filterId.equals(SEARCH_FILTER_ID)) {
				ArrayOfString filterValues = new ArrayOfString();
				filterValues.getItem().addAll(parameters.get(filterId));
				optionsType.setFilterId(SEARCH_FILTER_ID);
				optionsType.setFilterValueId(filterValues);
			} else if(filterId.equals(USERID_FILTER_ID)) {
				ArrayOfString filterValues = new ArrayOfString();
				filterValues.getItem().addAll(parameters.get(filterId));
				optionsType.setFilterId(USERID_FILTER_ID);
				optionsType.setFilterValueId(filterValues);
			} else if(filterId.equals(PRICE_FILTER_ID)) {
				RangeValueType filterRange = new RangeValueType();
				filterRange.setRangeValueMax(parameters.get(filterId).get(0));
				filterRange.setRangeValueMin(parameters.get(filterId).get(1));
				optionsType.setFilterId(PRICE_FILTER_ID);
				optionsType.setFilterValueRange(filterRange);
			}
			filterOptions.getItem().add(optionsType);
		}
		return filterOptions;
	}
	
}
