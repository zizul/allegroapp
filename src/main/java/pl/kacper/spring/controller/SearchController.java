package pl.kacper.spring.controller;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.kacper.spring.domain.Seller;
import pl.kacper.spring.service.InputStringValidator;
import pl.kacper.spring.service.SearchService;

@Controller
public class SearchController {

	private static final Logger logger = LoggerFactory.getLogger(SearchController.class);
	
	@Autowired
	@Qualifier("sellersForAllItemsSearchServiceImpl")
	private SearchService searchService;
	
	@Autowired
	@Qualifier("searchInputValidator")
	private InputStringValidator validator;
	
    @RequestMapping(value="/multisearch",method=RequestMethod.POST)
    public String multiSearch(@RequestParam("searchString") String searchString, Model model) {
    	if(validator.isValid(searchString)) {
    		List<Seller> sellers = searchService.search(Arrays.asList(searchString.split(",")));
	        if (sellers.size() < 1) {
	        	model.addAttribute("error", "NOT_FOUND");
	        } else {
	        	model.addAttribute("sellers", sellers);
	        	model.addAttribute("searchString", searchString);
	        }
    	} else
    		model.addAttribute("error", "NOT_VALID");
        return "multisearch";
    }
    
    @RequestMapping(value="/multisearch",method=RequestMethod.GET)
    public String multiSearch2() {
    	return "multisearch";
    }
}
