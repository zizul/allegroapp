package pl.kacper.spring.service;

public interface InputStringValidator {
	public Boolean isValid(String string);
}
