package pl.kacper.spring.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import pl.kacper.spring.domain.Seller;

@Service
public class SellersForAllItemsSearchServiceImpl extends SearchService {

	@Override
	protected List<Seller> searchByStrategy(List<String> searchStrings) {
		List<Set<Seller>> results = new ArrayList<Set<Seller>>(searchStrings.size());
		for (String item : searchStrings) {
			results.add(getSellerService().getSellersForItem(item));
		}
		for (int i = 1; i < results.size(); i++) {
			results.get(0).retainAll(results.get(i));
		}
		return new ArrayList<Seller>(!results.isEmpty() ? results.get(0) : new HashSet<Seller>());
		
	}

}
