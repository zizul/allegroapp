package pl.kacper.spring.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kacper.spring.domain.Item;
import pl.kacper.utils.ItemFactory;
import pl.kacper.ws.GetItemsListWebservice;
import pl.kacper.ws.RequestFilterOptionsService;
import pl.kacper.wsdl.DoGetItemsListResponse;
import pl.kacper.wsdl.ItemsListType;

@Service
public class ItemsServiceImpl implements ItemsService {

	private static final Logger logger = LoggerFactory
			.getLogger(ItemsServiceImpl.class);

	@Autowired
	private GetItemsListWebservice getItemsListWebservice;

	public List<Item> getItems(String searchString) {
		List<Item> items = new ArrayList<Item>();
		HashMap<String, List<String>> parameters = new HashMap<String, List<String>>();
		parameters.put(RequestFilterOptionsService.SEARCH_FILTER_ID,
				Arrays.asList(searchString));
		DoGetItemsListResponse response = getItemsListWebservice
				.getResponse(parameters);

		if (response.getItemsList() != null)
			for (ItemsListType result : response.getItemsList().getItem()) {
				items.add(ItemFactory.getItem(result));
			}
		return items;
	}

}
