package pl.kacper.spring.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kacper.spring.domain.Seller;
import pl.kacper.utils.SellerFactory;
import pl.kacper.ws.GetItemsListWebservice;
import pl.kacper.ws.RequestFilterOptionsService;
import pl.kacper.wsdl.DoGetItemsListResponse;
import pl.kacper.wsdl.ItemsListType;

@Service
public class SellerServiceImpl implements SellerService {

	private static final Logger logger = LoggerFactory.getLogger(SellerServiceImpl.class);
	
	@Autowired
	private GetItemsListWebservice getItemsListWebservice;
	
	public Set<Seller> getSellersForItem(String searchString) {

		HashSet<Seller> results = new HashSet<Seller>();
		HashMap<String, List<String>> parameters = new HashMap<String, List<String>>();
		parameters.put(RequestFilterOptionsService.SEARCH_FILTER_ID, Arrays.asList(searchString));
		
		DoGetItemsListResponse response = getItemsListWebservice.getResponse(parameters);
		if(response != null && response.getItemsList() != null)
			for (ItemsListType result : response.getItemsList().getItem()) {
				if(result.getSellerInfo() != null && result.getSellerInfo().getUserLogin() != null) {
					Seller seller = SellerFactory.getSeller(result);
					results.add(seller);
				}
			}
		
		return results;
	}
	
	public Boolean hasSellerTheItem(String searchString, Seller seller) {
		HashMap<String, List<String>> parameters = new HashMap<String, List<String>>();
		parameters.put(RequestFilterOptionsService.SEARCH_FILTER_ID, Arrays.asList(searchString));
		parameters.put(RequestFilterOptionsService.USERID_FILTER_ID, Arrays.asList(seller.getId().toString()));
		
		DoGetItemsListResponse response = getItemsListWebservice.getResponse(parameters);
		return response != null && response.getItemsCount() > 0;
	}
}
