package pl.kacper.spring.service;

import java.util.HashSet;
import java.util.Set;

import pl.kacper.spring.domain.Seller;

public interface SellerService {

	public Set<Seller> getSellersForItem(String searchString);
	
	public Boolean hasSellerTheItem(String searchString, Seller seller);
}
