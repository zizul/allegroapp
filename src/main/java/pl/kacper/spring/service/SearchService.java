package pl.kacper.spring.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kacper.spring.domain.Seller;

@Service
public abstract class SearchService {

	private static final Logger logger = LoggerFactory.getLogger(SearchService.class);
	
	private SellerService sellerService;
	
	@Autowired
	public void setSellerService(SellerService sellerService) {
		this.sellerService = sellerService;
	}

	public SellerService getSellerService() {
		return sellerService;
	}
	
	public List<Seller> search(List<String> items) {
		long start = System.currentTimeMillis();
		
		List<Seller> result = searchByStrategy(items);

		long end = System.currentTimeMillis();
		logger.debug("Time: " + (end - start) + " ms.");
		return result;
	}
	
	protected abstract List<Seller> searchByStrategy(List<String> items);

	
}
