package pl.kacper.spring.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class SearchInputValidator implements InputStringValidator {

	public Boolean isValid(String string) {
    	if(string != null && !string.isEmpty()) {
    		List<String> split = Arrays.asList(string.split(","));
    		if(split.isEmpty())
    			return false;
    		for (String str : split) {
    			str = str.replaceAll("\\s+", " ");
				if(str.equals("") || str.equals(" ") || !str.matches("[a-zA-Z0-9]+"))
					return false;
			}
    	} else{
    		return false;
    	}
		return true;
	}

}
