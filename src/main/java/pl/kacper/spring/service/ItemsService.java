package pl.kacper.spring.service;

import java.util.List;

import pl.kacper.spring.domain.Item;

public interface ItemsService {

	public List<Item> getItems(String searchString);
}
