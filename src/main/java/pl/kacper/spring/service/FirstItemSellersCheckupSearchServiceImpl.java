package pl.kacper.spring.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import pl.kacper.spring.domain.Seller;

@Service
public class FirstItemSellersCheckupSearchServiceImpl extends SearchService {

	@Override
	protected List<Seller> searchByStrategy(List<String> items) {
		Set<Seller> sellers = getSellerService().getSellersForItem(items.get(0));
		List<Seller> result = new ArrayList<Seller>();
		
		for (Seller seller : sellers) {
			boolean hasRestItems = true;
			int i = 1;
			while(hasRestItems && i < items.size()) {
				if(!getSellerService().hasSellerTheItem(items.get(i), seller))
					hasRestItems = false;
				i++;
			}
			if(hasRestItems)
				result.add(seller);
		}

		return result;
	}

}
