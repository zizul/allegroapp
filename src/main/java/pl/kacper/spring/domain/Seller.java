package pl.kacper.spring.domain;

public class Seller {
	private Integer id;
	private String login;

	public Seller() {
	}
	
	public Seller(Integer id, String login) {
		this.id = id;
		this.login = login;
	}
	
	public Seller(Integer id) {
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (!(obj instanceof Seller))
			return false;
		Seller seller = (Seller) obj;
		return seller.getId() != null && id.equals(seller.getId());
	}
}
