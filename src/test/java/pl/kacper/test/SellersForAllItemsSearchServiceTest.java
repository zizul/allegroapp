package pl.kacper.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;
import pl.kacper.spring.domain.Seller;
import pl.kacper.spring.service.SearchService;
import pl.kacper.spring.service.SellerService;
import pl.kacper.spring.service.SellersForAllItemsSearchServiceImpl;

public class SellersForAllItemsSearchServiceTest {

	private SearchService searchService;
	private SellerService sellerService;
	
	@Before
	public void setUp() throws Exception {
		searchService = new SellersForAllItemsSearchServiceImpl();
		sellerService = createMock(SellerService.class);
		searchService.setSellerService(sellerService);
	}

	@Test
	public void testSearch1Item() {
		List<String> items = createSearchItemList("monitor");
		expect(sellerService.getSellersForItem(items.get(0))).andReturn(createSellerSet(1, 2, 3, 4));

		replay(sellerService);

		List<Seller> result = searchService.search(items);
		assertNotNull(result);
		assertEquals(result.size(), 4);


		assertTrue(result.contains(new Seller(1)));
		assertTrue(result.contains(new Seller(2)));
		assertTrue(result.contains(new Seller(3)));
		assertTrue(result.contains(new Seller(4)));

		verify(sellerService);
	}
	
	@Test
	public void testSearch3Items2CommonSellers() {
		List<String> items = createSearchItemList("monitor", "komputer", "myszka");
		expect(sellerService.getSellersForItem(items.get(0))).andReturn(createSellerSet(1, 2, 3, 4));
		expect(sellerService.getSellersForItem(items.get(1))).andReturn(createSellerSet(2, 3, 4, 5));
		expect(sellerService.getSellersForItem(items.get(2))).andReturn(createSellerSet(3, 4, 5, 6));

		replay(sellerService);

		List<Seller> result = searchService.search(items);
		assertNotNull(result);
		assertEquals(result.size(), 2);

		assertTrue(result.contains(new Seller(3)));
		assertTrue(result.contains(new Seller(4)));

		verify(sellerService);
	}
	
	@Test
	public void testSearch3ItemsNoCommonSellers() {
		List<String> items = createSearchItemList("monitor", "komputer", "myszka");
		expect(sellerService.getSellersForItem(items.get(0))).andReturn(createSellerSet(1, 2, 3));
		expect(sellerService.getSellersForItem(items.get(1))).andReturn(createSellerSet(4, 5, 6));
		expect(sellerService.getSellersForItem(items.get(2))).andReturn(createSellerSet(7, 8, 9));
		
		replay(sellerService);

		List<Seller> result = searchService.search(items);
		assertNotNull(result);
		assertEquals(result.size(), 0);

		verify(sellerService);
	}
	
	@Test
	public void testSearch3ItemsFirstItemNoSeller() {
		List<String> items = createSearchItemList("monitor", "komputer", "myszka");
		expect(sellerService.getSellersForItem(items.get(0))).andReturn(createSellerSet());
		expect(sellerService.getSellersForItem(items.get(1))).andReturn(createSellerSet(4, 5, 6));
		expect(sellerService.getSellersForItem(items.get(2))).andReturn(createSellerSet(6, 7, 8));
		
		replay(sellerService);

		List<Seller> result = searchService.search(items);
		assertNotNull(result);
		assertEquals(result.size(), 0);

		verify(sellerService);
	}

	@Test
	public void testNoSearchItems() {
		List<Seller> result = searchService.search(new ArrayList<String>());
		assertNotNull(result);
		assertTrue(result.isEmpty());

	}
	
	@Test
	public void testEmptySearchItems() {
		List<String> items = createSearchItemList("");
		expect(sellerService.getSellersForItem(items.get(0))).andReturn(createSellerSet());
		
		replay(sellerService);
		List<Seller> result = searchService.search(items);
		assertNotNull(result);
		assertTrue(result.isEmpty());
		
		verify(sellerService);
	}
	
	@Test
	public void testNullSearchItems() {
		List<String> items = createSearchItemList(null, null);
		expect(sellerService.getSellersForItem(items.get(0))).andReturn(createSellerSet());
		expect(sellerService.getSellersForItem(items.get(0))).andReturn(createSellerSet());
		
		replay(sellerService);
		List<Seller> result = searchService.search(items);
		assertNotNull(result);
		assertTrue(result.isEmpty());
		
		verify(sellerService);
	}
	
	private Set<Seller> createSellerSet(Integer... ids) {
		Set<Seller> sellers = new HashSet<Seller>();
		for (Integer id : ids) {
			sellers.add(new Seller(id, "s"+id.toString()));
		}
		return sellers;
		
	}
	
	private List<String> createSearchItemList(String... strings) {
		List<String> items = new ArrayList<String>();
		for (String str : strings) {
			items.add(str);
		}
		return items;
	}

}
